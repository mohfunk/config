PS1="\[\e[32m\u\]  \[\e[36m\w\] \[\e[33m\]\[\e[1m\] \nλ \[\e[0m\]"
# Aliases
alias desktop='cd ~/Desktop'
alias ws='cd ~/ws'
alias rack='. ${HOME}/BAT/Rack/rack.bat'
alias home='cd ~/'
alias c='clear'
alias brc='vim ~/.bashrc'
alias vrc='vim ~/.vim/vimrc'
alias rebash='. ~/.bashrc'
alias ..='cd ..'
alias ls='ls -h --color'
alias lx='ls -lXB'      #  Sort by extension. 
alias lk='ls -lSr'      #  Sort by size, biggest last. 
alias lt='ls -ltr'      #  Sort by date, most recent last. 
alias lc='ls -ltcr'     #  Sort by/show change time,most recent last. 
alias lu='ls -ltur'     #  Sort by/show access time,most recent last.
alias todo='cat ~/ws/todo/todo.txt'
alias toadd='vim ~/ws/todo/todo.txt'
#Colors
BRed='\e[31m'         # Red
NC="\e[m"		# Color Rest

# Functions
function cd 
{     
	builtin cd "$@" && ls -F     
}
function _exit()        # Function to run upon exit of shell. 
{     
	echo -e "${BRed}Hasta la vista${NC}" 
} 
trap _exit EXIT
