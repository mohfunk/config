set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
call vundle#end()
filetype plugin indent on




:set number
augroup numbertoggle  	
	autocmd!  	
	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber  	
	autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber  
augroup END

set ruler
